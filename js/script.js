// В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.


let password = document.getElementById('password')
    , confirm_password = document.getElementById('confirm_password');
let submit = document.getElementById('btn');
submit.onclick = validatePassword;

function validatePassword() {
    if (password.value === confirm_password.value) {
        alert('You are welcome');
    } else {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'Please enter same passwords';

    }
}

let button = document.getElementById("showHidden");
button.onclick = show;
let input = document.getElementById("password");

function show() {
    if (input.getAttribute('type') === 'password') {
        input.setAttribute('type', 'text');
        button.setAttribute('class', 'eye fas fa-eye-slash');
    } else {
        input.setAttribute('type', 'password');
        button.setAttribute('class', 'eye fas fa-eye');
    }
}

let buttonConfirm = document.getElementById("showHiddenConfirm");
buttonConfirm.onclick = showConfirm;
let inputConfirm = document.getElementById("confirm_password");

function showConfirm() {
    if (inputConfirm.getAttribute('type') === 'password') {
        inputConfirm.setAttribute('type', 'text');
        buttonConfirm.setAttribute('class', 'eye fas fa-eye-slash');
    } else {
        inputConfirm.setAttribute('type', 'password');
        buttonConfirm.setAttribute('class', 'eye fas fa-eye');
    }
}